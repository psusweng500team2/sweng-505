USE smarthomedb
INSERT INTO LIGHTS (NAME, ON_CMD, OFF_CMD, GET_STATE_CMD) VALUES ('HueTest', 'echo "turn light on"', 'echo "turn light off"', 'echo "current light state"');
INSERT INTO SWITCHES (NAME, ON_CMD, OFF_CMD, GET_STATE_CMD) VALUES ('SwitchTest', 'echo "turn switch on"', 'echo "turn switch off"', 'echo "current switch state"');
INSERT INTO THERMOSTATS (NAME, GET_TEMP, SET_TEMP, FAN_ON_CMD, FAN_OFF_CMD) VALUES ('ThermTest', 'echo "get therm temp"', 'echo "set therm temp"', 'echo "turn fan on"', 'echo "turn fan off"');