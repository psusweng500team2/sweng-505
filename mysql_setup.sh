sudo apt-get install mysql-server python-mysqldb -y
# replace -ppassword with -pYOUR_PASSWORD_HERE
mysql -u root -ppassword < create_smarthome_user.sql
mysql --user=smarthome --password=password smarthomedb < build_tables.sql
mysql --user=smarthome --password=password smarthomedb < load_dummy_data.sql
