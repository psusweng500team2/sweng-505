USE smarthomedb
CREATE TABLE LIGHTS (
	ID 		INT NOT NULL AUTO_INCREMENT,
	NAME 		VARCHAR(255),
	ON_CMD 	VARCHAR(8000),
	OFF_CMD 	VARCHAR(8000),
	GET_STATE_CMD 	VARCHAR(8000),
	PRIMARY KEY (ID)
);

CREATE TABLE SWITCHES (
	ID 		INT NOT NULL AUTO_INCREMENT,
	NAME 		VARCHAR(255),
	ON_CMD 		VARCHAR(8000),
	OFF_CMD 	VARCHAR(8000),
	GET_STATE_CMD 	VARCHAR(8000),
	PRIMARY KEY (ID)
);


CREATE TABLE THERMOSTATS (
	ID 		INT NOT NULL AUTO_INCREMENT,
	NAME 		VARCHAR(255),
	GET_TEMP	VARCHAR(8000),
	SET_TEMP 	VARCHAR(8000),
	FAN_ON_CMD	VARCHAR(8000),
	FAN_OFF_CMD	VARCHAR(8000),
	PRIMARY KEY (ID)
);

COMMIT;
